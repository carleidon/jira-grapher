<a name="readme-top"></a>

<!-- PROJECT SHIELDS -->

[![MIT License][license-shield]][license-url]
[![Issues][issues-shield]][issues-url]
[![Last Commit][last-commit]][commits-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/carleidon/jira-grapher">
    <img src="./images/logo.png" alt="Logo" width="256" height="256">
  </a>

<h3 align="center">JIRA Grapher</h3>

  <p align="center">
    A webpage for generating graphs from JIRA. It uses the JIRA API and Chart.js library to generate graphs.
    <br />
    <a href="https://gitlab.com/carleidon/jira-grapher/-/wikis/home"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/carleidon/jira-grapher/-/issues/new"><img src="https://img.shields.io/badge/Report%20Bug-C04949" alt="Report bug" /></a>
    <a href="https://gitlab.com/carleidon/jira-grapher/activity"><img src="https://img.shields.io/badge/View%20Activity-85C049" alt="View activity" /></a>
  </p>
</div>

<!-- ABOUT THE PROJECT -->

## About The Project

The project is a webpage which displays a chart based on a JQL query. Currently Stacked Bar Chart, Doughnut Chart and Polar Area Chart could be genertated. More chart types will be implemented in the future, provided there are requirements.

This is currently in alpha, and the authentication feature has NOT yet been tested.

### Built Using

[![Chart.js][chart.js]][chart.js-url]

<!-- GETTING STARTED -->

## Getting Started

This project is a simple application that fetches data from a Jira API and visualizes it using Chart.js. Here's how to get started:

### Prerequisites

Before you begin, ensure you have met the following requirements:

-   You have a modern web browser that supports ES6 (e.g., Chrome, Firefox, Safari, Edge).
-   You have access to a Jira instance and have a username and API token.

### Setup

1. Clone this repository to your local machine.
2. Open the `index.html` file in your web browser.

### Configuration

You need to configure the application with your Jira domain, username, and API token. You can do this by modifying the `fetchData` function inside `script.js`:

```javascript
async function fetchData() {
	const jiraDomain = 'https://your-domain.atlassian.net'; // Replace with your Jira domain
	const username = 'your-username'; // Replace with your Jira username
	const apiToken = 'your-api-token'; // Replace with your Jira API token
	...
}
```

<!-- USAGE EXAMPLES -->

## Usage

Once you've set up and configured the application, simply load the `index.html` file in your web browser. The application will automatically fetch data from Jira and display a chart.

<!-- TROUBLESHOOTING -->

## Troubleshooting

If you encounter any errors, check the browser’s console for error messages. Ensure that your Jira domain, username, and API token are correct.
Please replace `'https://your-domain.atlassian.net'`, `'your-username'`, and `'your-api-token'` with your actual Jira domain, username, and API token respectively.

<!-- ROADMAP -->

## Roadmap

-   [ ] Fix the color handling by adding automatic color scheme generation.
-   [ ] Add more visualization JQL queries and their chart generation methods.
-   [ ] Improve the chart visualization with more customization options.
-   [ ] Implement the OAuth feature for a seamless and secure login experience.
-   [ ] Improve error handling by adding more user-friendly error messages.

See the [open issues](https://gitlab.com/carleidon/jira-grapher/-/issues) for a full list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch `git checkout -b feature/AmazingFeature`
3. Commit your Changes `git commit -m 'Add some AmazingFeature'`
4. Push to the Branch `git push origin feature/AmazingFeature`
5. Open a Pull Request

<!-- LICENSE -->

## License

This source code is licensed under the MIT license found in the [LICENSE](https://gitlab.com/carleidon/jira-grapher/-/blob/main/LICENSE) file in the root directory of this source tree.

<!-- MARKDOWN LINKS & IMAGES -->

[issues-shield]: https://img.shields.io/gitlab/issues/all/carleidon%2Fjira-grapher?color=%2324db24
[issues-url]: https://gitlab.com/carleidon/jira-grapher/-/issues
[license-shield]: https://img.shields.io/gitlab/license/carleidon/jira-grapher.svg?color=%23c4b7ff
[license-url]: https://gitlab.com/carleidon/jira-grapher/-/blob/main/LICENSE
[last-commit]: https://img.shields.io/gitlab/last-commit/carleidon%2Fjira-grapher?color=%2323a9dc
[commits-url]: https://gitlab.com/carleidon/jira-grapher/-/commits/main
[chart.js]: https://img.shields.io/badge/Chart.js-black?style=for-the-badge&logo=chart.js
[chart.js-url]: https://www.chartjs.org/
