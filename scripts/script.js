document.addEventListener('DOMContentLoaded', (event) => {
	const queries = [
		'project = "MARKASS" AND issuetype = Epic AND labels IN (AKG, OMV) ORDER BY priority DESC',
		'project = "MARKASS" AND issuetype = Epic AND labels IN (AKG) ORDER BY priority DESC',
		'project = "MARKASS" AND issuetype = Epic AND labels IN (OMV) ORDER BY priority DESC',
	];
	const chartTypes = ['bar', 'doughnut', 'polarArea'];
	queries.forEach((query, index) => {
		fetchData(query, index, chartTypes[index]);
	});
});

// The combine colors function
function combineColors(color1, color2) {
	let [r1, g1, b1, a1] = color1.match(/\d+\.?\d*/g);
	let [r2, g2, b2, a2] = color2.match(/\d+\.?\d*/g);
	let r = Math.sqrt((r1 ** 2 + r2 ** 2) / 2);
	let g = Math.sqrt((g1 ** 2 + g2 ** 2) / 2);
	let b = Math.sqrt((b1 ** 2 + b2 ** 2) / 2);
	let a = (parseFloat(a1) + parseFloat(a2)) / 2;
	return `rgba(${r},${g},${b},${a})`;
}

// The function to parse the JSON data received
function parseData(data) {
	let counts = {};
	let labelSet = new Set();
	let statusSet = new Set();

	for (let i = 0; i < data.issues.length; i++) {
		let issue = data.issues[i];
		let label = issue.fields.labels[0];
		let status = issue.fields.status.name;

		labelSet.add(label);
		statusSet.add(status);

		if (!counts[label]) {
			counts[label] = {};
		}

		if (!counts[label][status]) {
			counts[label][status] = 0;
		}

		counts[label][status]++;
	}

	let labels = Array.from(labelSet);
	let statuses = Array.from(statusSet);
	let datasets = [];

	let labelColors = [
		'rgba(255,99,132,0.5)',
		'rgba(54,162,235,0.5)',
		'rgba(255,206,86,0.5)',
		'rgba(75,192,192,0.5)',
		'rgba(153,102,255,0.5)',
		'rgba(255,159,64,0.5)',
	];

	let statusColors = [
		'rgba(255,99,132,0.5)',
		'rgba(54,162,235,0.5)',
		'rgba(255,206,86,0.5)',
		'rgba(75,192,192,0.5)',
		'rgba(153,102,255,0.5)',
		'rgba(255,159,64,0.5)',
	];

	for (let i = 0; i < labels.length; i++) {
		for (let j = 0; j < statuses.length; j++) {
			let label = labels[i];
			let status = statuses[j];
			let dataset = {
				label: `${label} - ${status}`,
				data: [],
				backgroundColor: combineColors(
					labelColors[i % labelColors.length],
					statusColors[j % statusColors.length]
				),
			};

			for (let k = 0; k < labels.length; k++) {
				let label = labels[j];
				dataset.data.push(k === i ? counts[label][status] || 0 : 0);
			}

			datasets.push(dataset);
		}
	}

	return {
		labels: labels,
		datasets: datasets,
	};
}

// The function to create the chart
function createChart(data, id, chartType) {
	let ctx = document.getElementById(id).getContext('2d');
	new Chart(ctx, {
		type: chartType,
		data: data,
		options: {
			indexAxis: chartType === 'bar' ? 'y' : 'x',
			scales: {
				x: {
					beginAtZero: true,
					stacked: chartType === 'bar',
					ticks: {
						fontColor: '#000',
						fontStyle: 'bold',
					},
				},
				y: {
					beginAtZero: true,
					stacked: chartType === 'bar',
					ticks: {
						fontColor: '#000',
						fontStyle: 'bold',
					},
				},
			},
			legend: {
				labels: {
					fontColor: '#000',
					fontSize: 18,
				},
			},
			animation: {
				duration: 750,
				easing: 'easeInOut',
			},
			tooltips: {
				enabled: true,
				backgroundColor: '#fff',
				borderColor: '#000',
				borderWidth: 1,
				titleFontColor: '#000',
				bodyFontColor: '#000',
			},
		},
	});
}

// Function to fetch the data asynchronously
async function fetchData(query, index, chartType) {
	const jiraDomain = 'https://your-domain.atlassian.net'; // The Jira domain
	const username = 'your-username'; // The Jira username
	const apiToken = 'your-api-token'; // The Jira API token

	const url = `${jiraDomain}/rest/api/2/search?jql=${encodeURIComponent(
		query
	)}&expand=fields`;

	const options = {
		method: 'GET',
		headers: {
			Authorization: `Basic ${btoa(`${username}:${apiToken}`)}`,
			Accept: 'application/json',
		},
	};

	try {
		const response = await fetch(url, options);
		const data = await response.json();
		const parsedData = parseData(data);

		const canvas = document.createElement('canvas');
		canvas.id = `jiraChart${index}`;
		document.querySelector('.container').appendChild(canvas);

		createChart(parsedData, canvas.id, chartType);
	} catch (error) {
		console.error('Error: ', error);
	}
}
